//single column toggle menu
(function($){
  $(function(options){
    var settings = $.extend({
      "wrapper": "#branding",
      "navToggle": "#toggleNav",
      "navToggleActiveClass": "active",
      "navList": "nav > .navwrap",
      "animationSpeed": "normal",
      "childClass": "hasChild",
      "childToggleClass": "toggleSub",
      "childToggleTitle": "toggle",
      "childToggleClassActive": "open",
      "breakpoint": "768"
    }, options );
    //console.log(settings);
    $(settings.navToggle).click(function(e) {
      $(settings.wrapper).find(settings.navList).slideToggle(settings.animationSpeed);
      $(settings.wrapper).find(settings.navToggle).toggleClass(settings.navToggleActiveClass);
      e.preventDefault();
    });
    $(settings.wrapper).find('li').has('ul').addClass(settings.childClass).prepend('<a href="#toggle" class="' + settings.childToggleClass + '">+</a>');
    $('.' + settings.childToggleClass).click(function (){
      $(this).toggleClass(settings.childToggleClassActive).find('~ ul').slideToggle();
      if ($(this).html() == '-') {
        $(this).html('+');
      } else {
        $(this).html('-');
      }
      return;
    });
    $(window).resize(function(){
      if ($(window).width() > settings.breakpoint ) {
        $(settings.wrapper).find('ul').removeAttr("style");
        $(settings.wrapper).find('a.' + settings.childToggleClassActive).removeClass(settings.childToggleClassActive);
        $('.' + settings.childToggleClass).html('+');
      }
    });
  });
}(jQuery));

//preload
$(window).load(function() {
  $("body").removeClass("preload");
});

//owl carousel
$(document).ready(function() {
  $('.owl-carousel').owlCarousel({
    margin:0,
    loop: true,
    lazyLoad: true,
    autoplaySpeed:1000,
    dotsSpeed:500,
    autoHeight:true,
    center:true,
    nav:false,
    dotsContainer: '#dots-container',
    responsive: {
      0: {
        items: 1
      }
    }
  })
})
(function(d) {
  var config = {
    kitId: 'rnx0jgy',
    scriptTimeout: 3000,
    async: true
  },
  h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
})(document);
